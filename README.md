# DC/OS Tutorials and Labs

This repo contains [DC/OS](https://dcos.io/) labs and tutorials authored both by Mesosphere, and by members of the community. We welcome contributions and want to grow the repo.

### About the Project
- [Introduction to DC/OS](https://youtu.be/JXrDQQe7Ud4)
- Setup instructions to install DC/OS in your machine with [Vagrant](https://github.com/dcos/dcos-vagrant) - links to a different repository

#### DC/OS tutorials:
* [Voting App](dcos-docker-votingapp/)
* [Flakio App](flakio/)

#### DC/OS Community tutorials
* Mesosphere [DC/OS Labs](https://github.com/dcos-labs) - links to a different repository
* [Marathon Recipies](https://github.com/mesosphere/marathon/blob/master/docs/docs/recipes.md) - links to a different repository

For more information on DC/OS, see the Official [DC/OS documentation](https://dcos.io/docs/latest/).

#### Contributing
We want to see this repo grow, so if you have a tutorial to submit please see this guide:

[Guide to submitting your own tutorial](contribute.md)